# Teensy Loader

An extremely simple Dockerfile to build and load Teensy programs. Teensy Loader
conains all of the tools needed to build for both the Arduino and Teensy
microcontrollers.

For those who wish to use make and custom build environments rather than use the
Arduino IDE. Teensy Loader uses the Arduino 1.8.8 IDE and teensyduino compiling
toolset.


To use:

```
docker pull dhuck/teensy_builder
```

Use Dockerfile.template inside of your project to automate your build process.
Set your project name within the template file and use the following commands to
build a make project:

```
docker build -t project_name .
docker run -v $PWD:/opt/project_name project_name make
```
