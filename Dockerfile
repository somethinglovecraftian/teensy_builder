FROM ubuntu:latest
MAINTAINER dhuck

ENV WORK_DIR=/opt/workspace
ENV PROJ_NAME=midi2dmx

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y -f wget \
            git make autoconf automake libncurses-dev flex gperf build-essential \
            libusb-dev python libfontconfig1 libudev-dev libxft2 


WORKDIR $WORK_DIR

RUN wget https://downloads.arduino.cc/arduino-1.8.13-linux64.tar.xz && \
    wget https://www.pjrc.com/teensy/td_153/TeensyduinoInstall.linux64 && \
    tar -xf arduino-1.8.13-linux64.tar.xz && \
    chmod 755 TeensyduinoInstall.linux64 && \
    ./TeensyduinoInstall.linux64 --dir=arduino-1.8.13 && \
    rm arduino-1.8.13-linux64.tar.xz

COPY ./tools ./arduino-1.8.13/hardware/tools/

RUN cd /opt/workspace && \
    git clone https://github.com/PaulStoffregen/teensy_loader_cli.git && \
    cd teensy_loader_cli && \
    make && \
    mv teensy_loader_cli /usr/bin

RUN apt-get clean && apt-get autoclean
